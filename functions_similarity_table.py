from prettytable import PrettyTable
import os
import time
#%%
def input_converter(input_file):
    """file input converted to list of strings"""

    with open(input_file, 'r', encoding='utf-8') as infile:
        text_stripped = [line.strip() for line in infile]

    return text_stripped


def simple_similarity_check(string_list):
    """converts the similarity_check output's values' to lists instead of dicts.
    Easier handling of similarity data is possible this way.
    If similarity value is 100, then value is substituted with '-' as own values
    are not interesting here."""


    """Create empty lists as dict values"""
    maindict = dict([x, []] for x in string_list)

    """Fill empty list with default 0s"""
    for base_string in string_list:
        base_list = [0 for x in range(len(string_list))]


        """minden listaelem, minden karaktere legyen összehasonlítva
        a többi listaelem azonos pozíciójú karakterével.
        Ha egyezés van, akkor a vizsgált listaelemhez tartozó pontok
        száma növekedjen. Ha a pontok száma = 100, akkor önmagával lett összehasonlítva"""
        for i in range(len(base_string)):
            for j in range(len(string_list)):
                if base_string[i] == string_list[j][i]:
                    base_list[j] += 100 / len(base_string)
                    if round(base_list[j]) == 100:
                        base_list[j] = '-'

        maindict[str(base_string)] = base_list

    return maindict

def similarity_check_dict(string_list: list):
    """
    Returns similarities in key:value pairs
    """
    maindict = dict([x, dict()] for x in string_list)

    """For every element of the string list a separate dictionary is created with
    {compared string name : similarity values as integers
    """
    for base_string in string_list:
        base_dict = dict([x, 0] for x in string_list)
        for i in range(len(base_string)):

            """Character by character comparison.Position matters!
            If 100% match: dict.value = 100
            """
            for element in string_list:
                if base_string[i] == element[i]:
                    base_dict[element] = base_dict.get(element) + \
                    int(100 / len(base_string))

        """When comparison iteration is over for a string,
        results are added to the output dictionary
        """
        maindict[base_string] = base_dict

    return maindict
#%%
def get_columns(simple_results: dict):

    columns = [k for k in simple_results.keys()]
    return columns


def get_rows(simple_results: dict):
    """Output is a list of 1 string and n integers"""

    rows = [[k] for k in simple_results.keys()]
    for enum, i in enumerate(simple_results.values()):
        for element in i:
            if type(element) == float:
                rows[enum].append(int(element))
            else:
                rows[enum].append(element)

    return rows


def create_table(columns: list, rows: list, name='table'):
    """Table records that are bigger than 0 are shown in %s

    Parameters:
        columns: get_columns() output
        rows: get_rows() output
        name: default table name
    """

    z = PrettyTable()

    """Oszlopnevek hozzáadása, táblázat elnevezése"""
    field_names = [name]
    field_names.extend(columns)

    z.field_names = field_names

    """Convert input:rows to percental values and add new value type
    to the new row_2 variable
    """
    new_rows = [[] for x in range(len(rows))]
    for enum, element in enumerate(rows):
        for i in rows[enum]:
            if type(i) is int and i > 0:
                new_rows[enum].append(str(i) + '%')
            else:
                new_rows[enum].append(i)

    for row in new_rows:
        z.add_row(row)

    return z
#%%
def is_exported (prettytable):
    """Returns True if already exported,
    Calls export_table if not"""
    exported_already = bool

    output_filename = str(prettytable.field_names[0]) + ".txt"
    if os.path.isfile(output_filename):
        exported_already = True
        print (output_filename, \
               "exportálás már megtörtént ekkor: ", \
               time.ctime(os.path.getmtime(output_filename)))
        print ("Ismételjük meg?", "\n")
        print ("(0) Nem")
        print ("(1) Igen")
        confirm = int(input("Válasz: "))
        #if confirm == 0:
         #   print ("...")
        if confirm == 1:
            export_table(prettytable)
            print ("Kész!")

    else:
        exported_already = False

    return exported_already


def export_table(prettytable):

    output_filename = str(prettytable.field_names[0]) + ".txt"
    output_text = prettytable.get_string()
    with open(output_filename, 'w') as file:
        file.write(output_text)
    print (output_filename, \
           "exportálása megtörtént.")

    return str(output_filename)

def open_table(prettytable):
    """
    The exported table can be openned externally,
    or within Python
    """

    output_filename = str(prettytable.field_names[0]) + ".txt"

    print ("Mivel nyissuk meg a táblázatot?", "\n")
    print ("(0) Text editor")
    print ("(1) Python")

    select_app = int(input("Válasz: "))
    if select_app == 0:
        if os.path.isfile(output_filename):
            os.startfile(output_filename)
        else:
            os.startfile(export_table(prettytable))
    elif select_app == 1:
        print(prettytable)

 #%%
def get_local_maximum(rows: list, reverse=False):
    """If we're lucky"""

    new_rows = [[] for x in range(len(rows))]
    for enum, row in enumerate(rows):
        new_rows[enum].append(row[0])

    max_1 = 0
    for enum, row in enumerate(rows):
        for i in row:
            if type(i) == str:
                if i == '-':
                    new_rows[enum].append('-')
                else:
                    continue
            elif type(i) == int or type(i) == float:
                if i > max_1:
                    max_1 = i
                    new_rows[enum].append(max_1)
                else:
                    new_rows[enum].append('-')
        max_1 = 0

    return new_rows


def keep_only_last_integer(lists: list):
    """Find last integer, and change rest of the elements
    to a '-' string"""

    for ls in lists:
        save = 0
        if type(ls[-1]) == int:
            save = ls[-1]
        else:
            for i in range(len(ls)):
                if type(ls[i]) == int and type(ls[i+1]) == str:
                    save = ls[i]
                    break
        for j in range(1,len(ls)):
            if ls[j] == save:
                continue
            else:
                ls[j] = '-'
    return lists

def ultimate_maximum(cleaned_rows: list):
    """Input: rows with only 1 integer
    Return the ultimate maximum"""

    list_of_maximums = [j for i in cleaned_rows for j in i if type(j) == int]
    maximum = max(list_of_maximums)

    candidate = []
    """Maximum is in one of the rows of cleaned_rows"""
    for enum, i in enumerate(cleaned_rows):
        if maximum in i:
            candidate.append(i[0])
            for num, j in enumerate(cleaned_rows[enum]):
                if j == maximum:
                    candidate.append(num)
                    break
    return candidate


def get_string_pair_from_column(candidate, columns):
    a = candidate[0]
    b = columns[candidate[1]-1]
    ls = [str(a), str(b)]
    return ls



