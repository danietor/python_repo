"""functions_similarty_table.py függvényeit hívja meg
Mindkét fájl a ..\advent\day_2 mappában van, abszolút címként hivatkozva
"""
import os
os.chdir("C:/Users/barnad/Documents/Python/advent/day_2/part_b")
import functions_similarity_table as f
#%%
def main_1():
    string_list = f.input_converter('input4.txt')
    simple_results = f.simple_similarity_check(string_list)
    rows = f.get_rows(simple_results)
    columns = f.get_columns(simple_results)
    table = f.create_table(columns, rows, name='table')
    
    """Table can be opened within Python, 
    or in external Text Editor. Export is neede for the latter.
    """
    f.is_exported(table)
    f.open_table(table)
#%%    
def main_2(): 
    string_list = f.input_converter('input2.txt')
    results = f.simple_similarity_check(string_list)
    
    """rows are cleaned from not-highest values"""
    rows = f.get_rows(results)
    max_local_values = f.get_local_maximum(rows)
    cleaned_rows = f.keep_only_last_integer(max_local_values)
    
    """table can now be created"""
    columns = f.get_columns(results)
    table = f.create_table(columns, cleaned_rows, name='table')
    f.open_table(table)
    f.is_exported(table)
    
    candidate = f.ultimate_maximum(cleaned_rows)
    final_candidate = f.get_string_pair_from_column(candidate, columns)
    for header, rows in zip(final_candidate[0], final_candidate[1]):  # loop over two lists
        if header != rows:
            print("{}: {}".format(header, ", ".join(rows)))


#%%
main_1()
main_2()